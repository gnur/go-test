package main

import "testing"

func Test_testfunc(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		arg  string
		want string
	}{
		{
			name: "world",
			arg:  "world",
			want: "hello world",
		},
		{
			name: "worlds",
			arg:  "worlds",
			want: "hello world",
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := testfunc(tt.arg); got != tt.want {
				t.Errorf("testfunc() = %v, want %v", got, tt.want)
			}
		})
	}
}
