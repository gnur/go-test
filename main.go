package main

import "fmt"

func main() {
	fmt.Println(testfunc("world"))
}

func testfunc(s string) string {
	return "hello " + s
}
